package com.zenika.academy.barbajavas.wordle.domain.model.game;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "games")
@Access(AccessType.FIELD)
public class Game {
    @Id
    private String tid;
    @Column
    private String word;
    @Column(name = "max_attempts")
    private int maxAttempts;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "round_results")
    @OrderColumn(name="round_order")
    @Column(name = "letters")
    private List<String> userGuesses;    
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    protected Game() {
        // For JPA
    }
    
    public Game(String tid, String word, int maxAttempts, User user) {
        this.user = user;
        this.tid = tid;
        this.word = word;
        this.maxAttempts = maxAttempts;
        this.userGuesses = new ArrayList<>(maxAttempts);
    }

    public String getTid() {
        return this.tid;
    }

    public Optional<User> getUser() {
        return Optional.ofNullable(this.user);
    }

    public int getAttemptsLeft() {
        return this.maxAttempts - this.getRounds().size();
    }

    public GameState getGameState() {
        if (this.getRounds().isEmpty()) {
            return GameState.IN_PROGRESS;
        } else if (this.getRounds().get(this.getRounds().size() - 1).isWin()) {
            return GameState.WIN;
        } else {
            return this.getRounds().size() < maxAttempts ? GameState.IN_PROGRESS : GameState.LOSS;
        }
    }

    public void guess(String userInput) throws GameNotForThisUserException {
        this.guess(userInput, null);
    }

    public void guess(String userInput, String userTid) throws GameNotForThisUserException {
        if(Objects.equals(this.user.getTid(), userTid)) {
            this.userGuesses.add(userInput);
        }
        else {
            throw new GameNotForThisUserException();
        }
    }

    public int getWordLength() {
        return word.length();
    }

    public List<RoundResult> getRounds() {
        return this.userGuesses.stream()
                .map(l -> RoundResult.fromGuess(this.word, l))
                .collect(Collectors.toList());
    }

    public String getWord() {
        return this.getGameState() == GameState.IN_PROGRESS ? "?" : this.word;
    }

    public String getWord(boolean force) {
        if (force) {
            return this.word;
        }
        else {
            return this.getWord();
        }
    }

    public Set<Character> getInvalidLetters() {
        return this.getRounds().stream()
                .flatMap(rr -> rr.invalidLetters().stream())
                .collect(Collectors.toSet());
    }

    public int getMaxAttempts() {
        return this.maxAttempts;
    }
}
