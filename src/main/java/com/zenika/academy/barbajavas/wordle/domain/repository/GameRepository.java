package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends CrudRepository<Game, String> {
    List<Game> findByUserTid(String userTid);
}
